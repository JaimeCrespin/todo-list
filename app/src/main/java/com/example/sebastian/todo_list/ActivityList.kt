package com.example.sebastian.todo_list

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import com.google.firebase.database.FirebaseDatabase

import kotlinx.android.synthetic.main.activity_list.*
import java.util.*

class ActivityList : AppCompatActivity(), ListSelectionRecyclerViewAdapter.ListSelectionRecyclerViewClickListener {

  override fun listitemClicked(todoList: TodoList) {
    showListDetail(todoList.id)
  }

  lateinit var listsRecyclerView: RecyclerView

  val database = FirebaseDatabase.getInstance()
  val ref = database.getReference("todo-list")


  companion object {
    val INTENT_LIST_ID = "listID"
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_list)
    setSupportActionBar(toolbar)

    // RecyclerView

    listsRecyclerView = findViewById(R.id.list_recycler_view)
    listsRecyclerView.layoutManager = LinearLayoutManager(this)
    listsRecyclerView.adapter = ListSelectionRecyclerViewAdapter(ref, this)


    fab.setOnClickListener { view ->
      showCreateListDialog()
    }
  }

  private fun showCreateListDialog() {

    val dialogTitle = getString(R.string.name_of_list)
    val positiveButtonTitle = getString(R.string.create_list)


    val builder = AlertDialog.Builder(this)
    val listTitleEditText = EditText(this)
    listTitleEditText.inputType = InputType.TYPE_CLASS_TEXT

    builder.setTitle(dialogTitle)
    builder.setView(listTitleEditText)


    builder.setPositiveButton(positiveButtonTitle) { dialog, i ->

      val newList = listTitleEditText.text.toString()
      val newId = UUID.randomUUID().toString()
      ref.child(newId).child("list-name").setValue(newList)

      dialog.dismiss()
      showListDetail(newId)
    }


    builder.create().show()

  }

  override fun onCreateOptionsMenu(menu: Menu): Boolean {
    // Inflate the menu; this adds items to the action bar if it is present.
    menuInflater.inflate(R.menu.menu_activity_list, menu)
    return true
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    return when (item.itemId) {
      R.id.action_settings -> true
      else -> super.onOptionsItemSelected(item)
    }
  }

  private fun showListDetail(listId:String) {
    val listDetailIntent = Intent(this, ListDetailActivity::class.java)

    listDetailIntent.putExtra(INTENT_LIST_ID,listId)

    startActivity(listDetailIntent)

  }
}



















